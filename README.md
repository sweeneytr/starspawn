# starspawn
Compression server implemented in Rust, providing dependency-injected
compression behavior behind a webserver.

# Target Platform
Rust 2018 edition or later, should run without platform-related trouble
on any rust
[tier-1](https://forge.rust-lang.org/release/platform-support.html#tier-1)
target, which includes Windows, Linux, and MacOS. Features used may be
compatible with 2015 edition, but such usage is untested.

Rust only has one compiler, `rustc`, which this project is compiled with. It
compiles rust down into LLVM IR (intermediate language), then uses the
existing IR -> asm backends to emit code. This explanation is colloquial, I'm
unfamiliar with finer details.

# How the code works
## Compression
Compression is abstracted behind the trait `compression::Compressor`, which
defines the  type-associated function `compress(..)` which takes a string
reference, and returns a new `String` object. As this is type associated,
it's trivial to dependency-inject a `Compressor`, which would otherwise require
boxing of dynamic objects.

The prefix compression is implemented via `compression::prefix::Prefix`, a unit
struct which implements `compression::Compressor` and is O(N) time and space.
It creates an iterator of `(char, count)` pairs from the input string, then
folds that iterator into an output string.

## Server Logic
Similarly, request and error processing is abstracted behind
`server::ServerBehavior`. The class `server::Server` implements a `run()` loop
awaiting connections in a loop, servicing each stream until that stream closes or
a `TcpListener::accept(..)` call fails, which won't happen unless the system
closes the underlying port.

The stream servicing loop awaits_requests (unpacking them from raw messages),
then passes them to `ServerBehavior::proc_request`, or
`ServerBehavior::proc_error` if an error occurred. `proc_error` gets a chance
to provide a response to be sent to the client, if it doesn't provide one the
server assumes the error was fatal and closes the stream, exiting the loop and
awaiting a new connection.

`server::prefix_behavior::PrefixBehavior` is another unit struct, which
implements the business logic of the server (e.g. mapping request to action and
response). It implements the behavior described in the prompt.

## Testing
Integration testing is done in parallel (albeit there are only two tests). 
`server:Server` can have a `halt_hook` added after construction, the test
infrastructure in `test::common` runs server instances in threads with these
hooks, then uses them at the end of test execution to halt the server and join
the thread back (no zombies here).

This infrastructure also ensures each test gets a unique port number, preventing
the double-bind collisions that would otherwise occur when two tests both
spawned a test server on the same port. This is implemented with an `AtomicU16`
that starts the range at 4001.

# 3rd party libraries
## Library Dependencies
* **getset** Automatically derive getters, setters, etc. Saves boilerplate.
* **log** Defines set of macros `debug, info, ...` that route to logging
  implementations, if any are instantiated, otherwise does nothing.
* **num** Using for `BigUint` for compression ratio tracking, rather than
  saturating and freezing when using some `uN` type.
* **num_enum** Derives `try_from<{integer}>, into<{integer}>` for enum types
  using the discriminant values.
* **packed_struct** Derives `packed_struct`, enabling pack-unpack from `u8`
  arrays, avoiding need for manual implementations of those functions.
* **packed_struct_codegen** Defines macros for automatically deriving a
  `packed_struct` from a struct declaration.
* **simplelog** Simple logging implementation, plumbs out to the terminal.
  End result is somewhat similar to raw `println!(...)`, but is future
  proofed against later migration.
* **structopt** Derives cli parsers from struct and enum declarations. Such
  a struct implements `::from_args()`, which returns a populated struct (or
  fails gracefully).
* **strum** Derives enum to static string methods, used to be more pretty
  when failing.
* **strum_macros** Derivation macros for the above.

## Test Libraries
* **lazy_static** Simplifies creating static singleton globals, used to create
  a global `AtomicU16` for unique ports while testing.

# Assumptions
* Clients don't send len-mismatched packets, e.g. the field `header.length`
  always matches the number of bytes received, regardless if they're valid.
  This failure will mislain all later messages received by the server over
  that stream.
* Stats commands (get-stats, reset-stats) are atomic. If message N is a
  get-stats request, the response captures stats for message 0..N-1. Similarly,
  if message N is a reset-stats, after responding it's stats are in their
  default state (e.g. the stats are reset **after** incrementing the sent
  counter).

# Improvements
* Magic-recovery could be implemented so that when a len-corrupted message is
  received the server can discard it and discover the next valid message.
* Server could be multithreaded relatively easily with a Mutex guarding
  the stats object.
* Tests could set up a virtual ethernet bridge so that they don't risk clobbering
  law-abiding programs that bind on localhost.
* Tests could be richer by man-in-the-middling traffic and stopping traffic at
  non-message boundaries.
* Client could be expanded into a REPL to exercise usage closer to what a real
  client would exhibit, but the provided integration testing in this module
  already is a better way to model and check the server's response.

# Custom Error Codes
The below error codes are all self-explanatory. `ResponseMessageTooLarge`
should never occur in practice, but is a theorizable failure and better to
enumerate than wrap into UnknownError.

33. BadMagic
34. BadCode
35. BodyLenMismatch
36. BodyDecodeFailed
37. ResponseMessageTooLarge