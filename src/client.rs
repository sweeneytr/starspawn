// builtin
use std::{
    net::{
        IpAddr,
        SocketAddr,
    },
    time::Duration,
};

// 3rd party
use log::error;
use simplelog::{
    SimpleLogger,
    LevelFilter,
    Config,
};
use structopt::*;

// internal
use starspawn::{
    client::{
        Client,
        Command,
    },
    Error,
    PORT,
};


#[derive(StructOpt)]
struct CommandOpt {
    /// Server to connect to.
    #[structopt(long, default_value="127.0.0.1")]
    host: IpAddr,
    /// TCP port number.
    #[structopt(long, default_value=PORT)]
    port: u16,
    /// Seconds to wait before connection timeout.
    #[structopt(long, default_value="2")]
    timeout: u64,
    #[structopt(subcommand)]
    command: Command,
}

#[derive(StructOpt)]
#[structopt(name = "starspawn-client")]
struct Opt {
    #[structopt(flatten)]
    command_opt: CommandOpt,

    #[structopt(long, default_value="warn")]
    level: LevelFilter,
}

fn send_command(opt: CommandOpt) -> Result<(), Error> {
    let sock = SocketAddr::new(opt.host, opt.port);
    let timeout = Duration::new(opt.timeout, 0);
    Client::connect(sock, timeout)?.proc_command(opt.command)
}

fn main() {
    let opt = Opt::from_args();
    SimpleLogger::init(opt.level, Config::default()).expect(
        "Couldn't set up logging");

    if let Err(e) = send_command(opt.command_opt) {
        error!("Error occured: {}", e);
    }
}