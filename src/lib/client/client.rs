// builtin
use std::{
    io,
    io::{
        Read,
        Write,
    },
    net::{
        TcpStream,
        Shutdown,
        SocketAddr,
    },
    time::Duration,
};

// 3rd party
use log::{
    error,
};

// internal
use crate::{
    Message,
    error::Error,
    Response,
    ResponseDeserializer,
};
use super::Command;


pub struct Client {
    stream: TcpStream,
}

impl Client {
    fn new(stream: TcpStream) -> Self {
        Self {
            stream,
        }
    }

    pub fn connect(sock: SocketAddr, timeout: Duration) -> io::Result<Client> {
        let stream = TcpStream::connect_timeout(&sock, timeout)
            .map_err(|e| {
                error!("Couldn't connect to server...");
                e
            })?;
        Ok(Self::new(stream))
    }

    pub fn shutdown(self) {
        self.stream.shutdown(Shutdown::Both).expect(
            "`shutdown` consumes value, no way to double-shutdown stream");
    }

    fn send_command<W: Write>(writer: &mut W, cmd: Command)
            -> Result<(), Error> {
        let msg: Message = cmd.into();
        msg.write(writer).map_err(|e| {
            error!("Couldn't send request...");
            e.into()
        })
    }

    fn await_response<R: Read>(reader: &mut R, deserializer: ResponseDeserializer)
            -> Result<Response, Error> {
        let msg = Message::read(reader).map_err(|e| {
            error!("Error recieving response...");
            e
        })?;

        Ok(Response::deserialize(deserializer, msg)?)
    }

    pub fn proc_command(&mut self, cmd: Command) -> Result<(), Error> {
        let parser = cmd.get_parser();
        Self::send_command(&mut self.stream, cmd)?;
        match Self::await_response(&mut self.stream, parser) {
            Ok(rsp) => println!("{:?}", rsp),
            _ => error!("Recieved malformed response..."),
        }

        Ok(())
    }

    pub fn commands(&mut self, commands: Vec<Command>) -> Result<(), Error> {
        for command in commands {
            self.proc_command(command)?;
        }

        Ok(())
    }
}