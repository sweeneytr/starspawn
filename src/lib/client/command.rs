// 3rd party
use structopt::StructOpt;

// internal
use crate::{
    Message,
    Request,
    Response,
    ResponseDeserializer,
};


#[derive(Clone)]
#[derive(StructOpt)]
pub enum Command {
    #[structopt(flatten)]
    Request(Request),
    /// Send a corrupted message to the server.
    Corrupt,
}

impl Command {
    pub fn get_parser(&self) -> ResponseDeserializer {
        match self {
            Command::Request(req) => req.response_deserializer(),
            Command::Corrupt => Response::deserialize_empty,
        }
    }
}

impl From<Command> for Message {
    fn from(cmd: Command) -> Self {
        match cmd {
            Command::Request(req) => req.into(),
            Command::Corrupt => Message::new(5, None),
        }
    }
}