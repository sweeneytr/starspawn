// internal
mod prefix;
pub use self::prefix::Prefix;


pub trait Compressor {
    fn compress<S>(&mut self, string: S) -> String where S: AsRef<str>;
}