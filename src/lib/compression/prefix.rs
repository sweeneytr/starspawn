// internal
use super::Compressor;


/* Defining an iterator which reduces a Iter<char> to (char, n) pairs
 * allows a nicer .fold() into a string later, and is more ideompotent,
 * avoiding creating some queue of (char, n) values to later reduce.
 */

 struct PrefixIterator<I>
 where
     I: Iterator<Item=char>,
 {
     /* Peak avoids having to  internally track `cur_char` */
     iter: std::iter::Peekable<I>,
 }
 
 /* repeated where's aren't so DRY,  RFC 2089 introduces implied bounds, such
  * that the compiler can deduce from the trait bounds on the struct
  * (e.g. the struct's where statement)  that the impl also has this bound.
  * 
  * https://github.com/rust-lang/rfcs/blob/master/text/2089-implied-bounds.md
  */
 
 impl<I> PrefixIterator<I> 
 where
     I: Iterator<Item=char>
 {
     pub fn new(iter: I) -> Self {
         Self {
             iter: iter.peekable(),
         }
     }
 }
 
 impl<I> Iterator for PrefixIterator<I>
 where
     I: Iterator<Item=char>,
 {
     type Item = (char, usize);
 
     fn next(&mut self) -> Option<Self::Item> {
         let cur_char = match self.iter.next() {
             Some(c) => c,
             _ => return None,
         };
         let mut count = 1;
 
         loop {
             match self.iter.peek() {
                 Some(c) if *c == cur_char => {
                     self.iter.next(); // Discard value
                     count += 1;
                 },
                 _ => return Some((cur_char, count)),
             }
         }
     }
 }

pub struct Prefix;

impl Compressor for Prefix {
    fn compress<S>(&mut self, string: S) -> String
    where
        S: AsRef<str>
    {
        let string = string.as_ref();
        /* It may be more ideomatic to have PrefixIterator return string chunks,
        * and collect that, but the below feels more performant.
        */
        PrefixIterator::new(string.chars())
            /* only allocates memory once, provable by induction that a compressed
            * string is never larger than an uncompressed one.
            */
            .fold(String::with_capacity(string.len()), |mut s, pair| {
                if pair.1 > 1 {
                    s.push_str(&format!("{}{}", pair.1, pair.0));
                } else {
                    s.push(pair.0);
                }
                s
            })
    }
}

 #[cfg(test)]
 mod tests {
     use super::*;
 
     #[test]
     fn nominal() {
         assert_eq!(Prefix.compress(""), "");
         assert_eq!(Prefix.compress("aaaabbbbcccc"), "4a4b4c");
         assert_eq!(Prefix.compress("baaaacbbbbaccccb"), "b4ac4ba4cb");
     }
 }