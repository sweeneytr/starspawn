// builtin
use std::{
    fmt,
    io,
};

// internal
use crate::{
    PackError,
    ReadError,
    UnpackError,
    WriteError,
};


#[derive(Debug)]
pub enum Error {
    IO(io::Error),
    Pack(PackError),
    Unpack(UnpackError),
}

impl From<io::Error> for Error {
    fn from(err: io::Error) -> Self {
        Self::IO(err)
    }
}

impl From<PackError> for Error {
    fn from(err: PackError) -> Self {
        Self::Pack(err)
    }
}

impl From<UnpackError> for Error {
    fn from(err: UnpackError) -> Self {
        Self::Unpack(err)
    }
}

impl From<ReadError> for Error {
    fn from(err: ReadError) -> Self {
        match err {
            ReadError::IO(e) => e.into(),
            ReadError::Unpack(e) => e.into(),
        }
    }
}

impl From<WriteError> for Error {
    fn from(err: WriteError) -> Self {
        match err {
            WriteError::IO(e) => e.into(),
            WriteError::Pack(e) => e.into(),
        }
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let displayable: &dyn fmt::Display = match self {
            Self::IO(err) => err,
            Self::Pack(err) => err,
            Self::Unpack(err) => err,
        };
        displayable.fmt(f)
    }
}