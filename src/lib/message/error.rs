// builtin
use std::{
    fmt,
    io,
};

// 3rd party
use strum::AsStaticRef;


#[derive(Debug)]
#[derive(AsStaticStr)]
pub enum PackError {
    BodyTooLarge,
}

impl fmt::Display for PackError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Pack Error: {}", self.as_static())
    }
}

pub enum WriteError {
    IO(io::Error),
    Pack(PackError),
}

impl fmt::Display for WriteError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let displayable: &dyn fmt::Display = match self {
            Self::IO(e) => e,
            Self::Pack(e) => e,
        };
        
        displayable.fmt(f)
    }
}

impl From<io::Error> for WriteError {
    fn from(err: io::Error) -> Self {
        Self::IO(err)
    }
}

impl From<PackError> for WriteError {
    fn from(err: PackError) -> Self {
        Self::Pack(err)
    }
}

#[derive(Debug)]
#[derive(AsStaticStr)]
pub enum UnpackError {
    BadMagic,
    BadCode,
    BodyTooLarge,
    BodyLenMismatch,
    BodyDecodeFailed,
}

impl fmt::Display for UnpackError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Unpack Error: {}", self.as_static())
    }
}

pub enum ReadError {
    IO(io::Error),
    Unpack(UnpackError),
}

impl fmt::Display for ReadError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let displayable: &dyn fmt::Display = match self {
            Self::IO(e) => e,
            Self::Unpack(e) => e,
        };
        
        displayable.fmt(f)
    }
}

impl From<io::Error> for ReadError {
    fn from(err: io::Error) -> Self {
        Self::IO(err)
    }
}

impl From<UnpackError> for ReadError {
    fn from(err: UnpackError) -> Self {
        Self::Unpack(err)
    }
}