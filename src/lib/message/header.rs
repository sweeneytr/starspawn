// builtin
use std::{
    convert::{
        TryInto,
        TryFrom,
    },
    io::{
        Read,
        Write,
    },
};

// 3rd party
use num::Integer;
use packed_struct::{
    PackedStruct,
    PackedStructInfo,
};

// Internal
use super::{
    PackError,
    ReadError,
    UnpackError,
    WriteError,
};


const LENGTH_LIMIT: u16 = 32 * 1024;
const MAGIC: u32 = 0x53545259;

#[derive(PackedStruct)]
#[packed_struct(endian="msb")]
pub struct Header {
    magic: u32,
    length: u16,
    code: u16,
}

impl Header {
    pub fn new(code: u16, length: u16) -> Self {
        Self {
            magic: MAGIC,
            code,
            length,
        }
    }

    pub fn packed_bytes() -> usize {
        let (div, rem) = Self::packed_bits().div_rem(&8);
        div + if rem > 0 { 1 } else { 0 }
    }

    pub fn read<R: Read>(reader: &mut R) -> Result<Self, ReadError> {
        let mut buf = vec![0; Self::packed_bytes()];
        reader.read_exact(buf.as_mut_slice())?;
        
        let arr = buf.as_slice().try_into().expect(
            "Can always create arr from slice of same size");
        
        Ok(Self::unpack(arr).expect(
            "unpack() has no failure mode for struct of primitives"))
    }

    pub fn write<W: Write>(&self, writer: &mut W) -> Result<(), WriteError> {
        if self.length > LENGTH_LIMIT {
            Err(PackError::BodyTooLarge.into())
        } else {
            writer.write_all(&self.pack())?;
            Ok(())
        }
    }
}

impl TryFrom<Header> for (u16, u16) {
    type Error = UnpackError;
    fn try_from(header: Header) -> Result<Self, Self::Error> {
        if header.magic != MAGIC {
            Err(UnpackError::BadMagic)
        } else if header.length > LENGTH_LIMIT {
            Err(UnpackError::BodyTooLarge)
        } else {    
            Ok((header.code, header.length))
        }
    }
}
