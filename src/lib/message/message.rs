
// builtin
use std::{
    convert::{
        TryInto,
        TryFrom,
    },
    io::{
        Read,
        Write,
    },
};

// Internal
use crate::{
    RequestCode,
    ResponseCode,
};
use super::{
    header::Header,
    error::{
        PackError,
        ReadError,
        UnpackError,
        WriteError,
    },    
};


pub struct Message {
    code: u16,
    body: Option<Vec<u8>>,
}

impl Message {
    pub fn new(code: u16, body: Option<Vec<u8>>) -> Self {
        Self {
            code,
            body,
        }
    }

    pub fn packed_bytes(&self) -> usize {
        let len = self.body.as_ref().map_or(0, |vec| vec.len());
        Header::packed_bytes() + len
    }

    pub fn read<R: Read>(reader: &mut R) -> Result<Self, ReadError> {
        let header = Header::read(reader)?;
        let (code, len): (u16, u16) = header.try_into()?;
        Ok(Self {
            code,
            body: if len == 0 { None } else {
                let mut buf = vec![0; len.into()];
                reader.read_exact(buf.as_mut_slice())?;
                Some(buf)
            }
        })
    }
    
    pub fn write<W: Write>(&self, writer: &mut W) -> Result<(), WriteError> {
        let len = self.body.as_ref().map_or(0, |vec| vec.len()).try_into()
            .map_err(|_| PackError::BodyTooLarge)?;

        Header::new(self.code, len).write(writer)?;
        if let Some(body) = &self.body {
            writer.write_all(body.as_slice())?;
        } 

        Ok(())
    }
}

impl TryFrom<Message> for (RequestCode, Option<Vec<u8>>) {
    type Error = UnpackError;

    fn try_from(msg: Message) -> Result<Self, Self::Error> {
        Ok((msg.code.try_into().map_err(|_| UnpackError::BadCode)?, msg.body))
    }
}

impl TryFrom<Message> for (ResponseCode, Option<Vec<u8>>) {
    type Error = UnpackError;

    fn try_from(msg: Message) -> Result<Self, Self::Error> {
        Ok((msg.code.try_into().map_err(|_| UnpackError::BadCode)?, msg.body))
    }
}