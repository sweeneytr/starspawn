// internal
mod error;
mod header;
mod message;
pub use self::{
    error::{
        PackError,
        ReadError,
        UnpackError,
        WriteError,
    },
    message::Message,
};
