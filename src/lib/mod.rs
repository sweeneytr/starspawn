// 3rd party
/* Rust 2018 removes need for #[macro_use] for macros, but requires
 * migration from old-styple procmacros, which the below haven't migrated
 * from.
 */
#[macro_use]
extern crate packed_struct_codegen;
#[macro_use]
extern crate strum_macros;
#[macro_use]
extern crate getset;

// internal
pub mod client;
pub mod compression;
pub mod error;
pub mod server;
mod message;
mod request;
mod response;
pub use self::{
    error::Error,
    request::{
        RequestCode,
        Request,
    },
    message::{
        Message,
        PackError,
        ReadError,
        UnpackError,
        WriteError,
    },
    response::{
        Response,
        ResponseCode,
        ResponseDeserializer,
        Stats,
    },
};


pub const PORT: &'static str = "4000";