// builtin
use std::convert::{
    TryFrom,
    TryInto,
};

// 3rd party
use num_enum::{
    IntoPrimitive,
    TryFromPrimitive,
};
use structopt::StructOpt;

// internal
use crate::{
    Message,
    Response,
    ResponseDeserializer,
    UnpackError,
};


#[derive(Clone)]
#[derive(StructOpt)]
pub enum Request {
    /// Ping the server to check it's health.
    Ping,
    /// Get the statistics of the server. These include number of bytes sent
    /// and recieved, and the compression ratio achived.
    GetStats,
    /// Reset the runtime statistics the server is tracking.
    ResetStats,
    /// Send a string to the server for compression via prefix encoding, e.g.
    /// aaabbbccc -> 3a3b3c. The server can compress any ascii that doesn't
    /// have numerics in it, but rejects any ascii outside the range a-z for
    /// compatability.
    Compress{string: String},
}

#[derive(IntoPrimitive, TryFromPrimitive)]
#[derive(PartialEq)]
#[repr(u16)]
pub enum RequestCode {
    Ping = 1,
    GetStats = 2,
    ResetStats = 3,
    Compress = 4,
}

impl Request {
    // Response parsing is context sensitive w.r.t. sent Request 
    pub fn response_deserializer(&self) -> ResponseDeserializer {
        match self {    
            Self::Ping | Self::ResetStats => Response::deserialize_empty,
            Self::GetStats => Response::deserialize_stats,
            Self::Compress{string: _} => Response::deserialize_string,
        }
    }
}

impl From<Request> for Message {
    fn from(request: Request) -> Self {
        match request {
            Request::Ping => Message::new(RequestCode::Ping.into(), None),
            Request::GetStats => Message::new(RequestCode::GetStats.into(), None),
            Request::ResetStats => Message::new(RequestCode::ResetStats.into(), None),
            Request::Compress{string} => Message::new(RequestCode::Compress.into(), Some(string.into_bytes())),
        }
    }
}

impl TryFrom<Message> for Request {
    type Error = UnpackError;

    fn try_from(msg: Message) -> Result<Self, Self::Error> {
        let (code, body) = msg.try_into()?;

        match code {
            RequestCode::Compress => {
                let body = body.ok_or(UnpackError::BodyLenMismatch)?;
                let string = String::from_utf8(body)
                    .map_err(|_| UnpackError::BodyDecodeFailed)?;
                Ok(Self::Compress{
                    string,
                })
            },

            /* Match is fallthrough, matches after this know body.is_none() */
            _ if body.is_some() => Err(UnpackError::BodyLenMismatch).into(),
            RequestCode::Ping => Ok(Self::Ping),
            RequestCode::GetStats => Ok(Self::GetStats),
            RequestCode::ResetStats => Ok(Self::ResetStats),
        }
    }
}