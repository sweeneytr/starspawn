// builtin
use std::convert::{
    TryInto,
};

// 3rd party
use num_enum::{
    IntoPrimitive,
    TryFromPrimitive,
};
use packed_struct::prelude::*;

// internal
use crate::{
    Message,
    PackError,
    UnpackError,
};


#[derive(Clone, Copy, Debug, PartialEq)]
#[derive(IntoPrimitive, TryFromPrimitive)]
#[repr(u16)]
pub enum ResponseCode {
    /* required responses */
    Ok = 0,
    UnknownError,
    MessageTooLarge,
    UnsupportedRequestType,
    /* 4 - 32 reserved */
    /* 33 - 128 impl defined */
    BadMagic = 33,
    BadCode,
    BodyLenMismatch,
    BodyDecodeFailed,
    ResponseMessageTooLarge,
}

impl From<UnpackError> for ResponseCode {
    fn from(e: UnpackError) -> Self {
        match e {
            UnpackError::BadMagic => Self::BadMagic,
            UnpackError::BadCode => Self::BadCode,
            UnpackError::BodyTooLarge => Self::MessageTooLarge,
            UnpackError::BodyLenMismatch => Self::BodyLenMismatch,
            UnpackError::BodyDecodeFailed => Self::BodyDecodeFailed,
        }
    }
}

impl From<PackError> for ResponseCode {
    fn from(e: PackError) -> Self {
        match e {
            PackError::BodyTooLarge => Self::ResponseMessageTooLarge,
        }
    }
}

#[derive(Debug, Default, Clone)]
#[derive(Getters)]
#[derive(PackedStruct)]
#[packed_struct(endian="msb")]
pub struct Stats {
    #[getset(get = "pub")]
    recieved: u32,
    #[getset(get = "pub")]
    sent: u32,
    ratio: u8,
}

impl Stats {
    pub fn new(sent: u32, recieved: u32, ratio: u8) -> Self {
        Self {
            recieved,
            sent,
            ratio,
        }
    }
}

#[derive(Debug)]
pub enum Response {
    Empty(ResponseCode),
    Stats(Stats),
    String(String),
}

type UnpackResult = Result<Response, UnpackError>;
pub type ResponseDeserializer = fn(Option<Vec<u8>>) -> UnpackResult;

impl Response {
    pub fn deserialize_empty(body: Option<Vec<u8>>) -> UnpackResult {
        if body.is_some() {
            Err(UnpackError::BodyLenMismatch)
        } else {
            Ok(Response::Empty(ResponseCode::Ok))
        }
    }

    pub fn deserialize_stats(body: Option<Vec<u8>>) -> UnpackResult {
        body.ok_or(UnpackError::BodyLenMismatch).and_then(|vec| {
            vec.as_slice().try_into().map_err(|_| UnpackError::BodyLenMismatch)
                .and_then(|arr| Stats::unpack(arr).map_err(|_| UnpackError::BodyLenMismatch)
                    .map(|stats| Response::Stats(stats)))
        })
    }

    pub fn deserialize_string(body: Option<Vec<u8>>) -> UnpackResult {
        body.ok_or(UnpackError::BodyLenMismatch).and_then(|vec| {
            String::from_utf8(vec).map(|string| Response::String(string))
                .map_err(|_| UnpackError::BodyDecodeFailed)
        })
    }

    pub fn deserialize(deserializer: ResponseDeserializer, msg: Message)
            -> Result<Self, UnpackError>
    {
        let (code, body) = msg.try_into()?;
        if code == ResponseCode::Ok {
            deserializer(body)
        } else if body.is_some() {
            /* error code responses should always be empty */
            Err(UnpackError::BodyLenMismatch)
        } else { 
            Ok(Response::Empty(code))
        }
    }
}

impl From<Response> for Message {
    fn from(response: Response) -> Self {
        match response {
            Response::Empty(code) => Message::new(code.into(), None),
            Response::Stats(stats) => Message::new(ResponseCode::Ok.into(), Some(stats.pack().to_vec())),
            Response::String(string) => Message::new(ResponseCode::Ok.into(), Some(string.into())),
        }
    }    
}