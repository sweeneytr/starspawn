// internal
mod prefix_behavior;
mod server;
mod stats;
pub use self::{
    stats::Stats,
};
use self::{
    prefix_behavior::PrefixBehavior,
    stats::StatsUpdate,
};
use crate::{
    Error,
    Request,
    Response,
};

pub type Server = server::Server<PrefixBehavior>;

type ProcResult = (Response, Option<StatsUpdate>);
pub trait ServerBehavior {
    fn proc_request(request: Request, stats: &Stats) -> ProcResult;
    fn proc_error(err: Error) -> Option<ProcResult>;
}