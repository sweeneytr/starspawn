// builtin
use std::{
    io::{
        ErrorKind,
    },
};

// 3rd party
use log::{
    info,
    error,
};

// internal
use crate::{
    compression::{
        Compressor,
        Prefix,
    },
    error::Error,
    Request,
    Response,
    ResponseCode,
};
use super::{
    ProcResult,
    stats::{
        Stats as ServerStats,
        StatsUpdate,
    },
    ServerBehavior,
};


pub struct PrefixBehavior;

impl ServerBehavior for PrefixBehavior {
    fn proc_request(request: Request, stats: &ServerStats) -> ProcResult {
        match request {
            Request::Ping => {
                info!("Pinged...");
                (Response::Empty(ResponseCode::Ok), None)
            },
            Request::Compress{string} => {
                info!("Compressing string...");
                let compressed = Prefix.compress(&string);
                let update = StatsUpdate::Compression(string.len(), compressed.len());
                (Response::String(compressed), Some(update))
            },
            Request::GetStats => {
                info!("Returning stats...");
                (Response::Stats(stats.into()), None)
            },
           Request::ResetStats => {
                info!("Resetting stats...");
                (Response::Empty(ResponseCode::Ok), Some(StatsUpdate::Reset))
            },
        }
    }

    fn proc_error(err: Error) -> Option<ProcResult> {
        match err {
            Error::IO(e) => match e.kind() {
                ErrorKind::UnexpectedEof => None,
                _ => {
                    error!("Await request failed: {}", e);
                    None
                },
            },
            Error::Unpack(e) => {
                error!("{}", e);
                Some((Response::Empty(e.into()), None))
            },
            Error::Pack(e) => {
                error!("{}", e);
                Some((Response::Empty(e.into()), None))
            }, 
        }
    }
}