// builtin
use std::{
    convert::TryInto,
    io,
    io::{
        Read,
        Write,
    },
    marker::PhantomData,
    net::{
        TcpListener,
        SocketAddr,
    },
};

// 3rd party
use log::{
    debug,
    info,
    error,
};

// internal
use crate::{
    error::Error,
    Message,
    Request,
    Response,
    UnpackError,
};
use super::{
    ServerBehavior,
    stats::{
        Stats as ServerStats,
        StatsUpdate,
    },
};


pub struct Server<B>
where
    B: ServerBehavior,
{
    sock: SocketAddr,
    stats: ServerStats,

    behavior: PhantomData<B>,
    halt_hook: Option<Box<dyn Fn() -> bool>>,
}

impl<B> Server<B>
where
    B: ServerBehavior,
{
    pub fn new(sock: SocketAddr) -> Self {
        Self {
            sock,
            stats: ServerStats::default(),

            behavior: PhantomData,
            halt_hook: None,
        }
    }

    pub fn add_halt_hook(&mut self, halt_hook: Box<dyn Fn() -> bool>)
            -> Result<(), ()> {
        match self.halt_hook {
            None => {
                self.halt_hook = Some(halt_hook);
                Ok(())
            },
            _ => Err(()),
        }
    }

    fn await_request<R: Read>(&mut self, reader: &mut R)
            -> (Result<Request, Error>, Option<StatsUpdate>) {
        let msg = match Message::read(reader) {
            Ok(msg) => msg,
            Err(e) => return (Err(e.into()), None),
        };
        let update = StatsUpdate::Recieved(msg.packed_bytes());
        (msg.try_into().map_err(|e: UnpackError| e.into()), Some(update))
    }

    fn send_response<W: Write>(&mut self, writer: &mut W, rsp: Response)
            -> Result<StatsUpdate, Error> {
        let msg: Message = rsp.into();
        let update = StatsUpdate::Sent(msg.packed_bytes());
        msg.write(writer)?;
        Ok(update)
    }

    fn halt(&self) -> bool {
        self.halt_hook.as_ref().map_or(false, |halt| halt())
    }

    pub fn run(mut self) -> io::Result<()> {
        let listener = TcpListener::bind(&self.sock)?;
        while !self.halt() {
            let (mut stream, sock) = listener.accept().map_err(|e| {
                error!("Failed to open stream: {}", e);
                e
            })?;
            info!("Client connected {}", sock);

            loop {
                let (rsp, req_update, proc_update) = match self.await_request(&mut stream) {
                    (Ok(req), req_update) => {
                        let (rsp, proc_update) = B::proc_request(req, &self.stats);
                        (rsp, req_update, proc_update)
                    },
                    (Err(e), req_update) => {
                        match B::proc_error(e) {
                            Some((rsp, proc_update)) => {
                                info!("Sending error response...");
                                (rsp, req_update, proc_update)
                            },
                            _ => break,
                        }
                    },
                };


                let rsp_update = match self.send_response(&mut stream, rsp) {
                    Ok(update) => update,
                    Err(e) => {
                        error!("Send response failed... {}", e);
                        break;
                    },
                };

                if let Some(update) = req_update {                
                    self.stats.apply_update(update);
                }
                self.stats.apply_update(rsp_update);

                if let Some(update) = proc_update {
                    self.stats.apply_update(update);
                }
            }
            
            debug!("Closing stream...");
        }

        Ok(())
    }
}