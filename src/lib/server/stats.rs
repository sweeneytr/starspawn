// builtin
use std::convert::TryInto;

// 3rd party
use getset::CopyGetters;
use num::BigUint;


pub enum StatsUpdate {
    Recieved(usize),
    Sent(usize),
    Compression(usize, usize),
    Reset,
}

#[derive(Debug, Default, Clone)]
#[derive(CopyGetters)]
pub struct Stats {
    #[getset(get_copy = "pub")]
    recieved: u32,
    #[getset(get_copy = "pub")]
    sent: u32,
    compressed: BigUint,
    uncompressed: BigUint,
}

impl Stats {
    pub fn apply_update(&mut self, update: StatsUpdate) {
        let into_u32_or_max = |x: usize| {
            x.try_into().unwrap_or(u32::max_value())
        };

        match update {
            StatsUpdate::Recieved(x) => {
                self.recieved = self.recieved.saturating_add(into_u32_or_max(x))
            },
            StatsUpdate::Sent(x) => {
                self.sent = self.sent.saturating_add(into_u32_or_max(x))
            },
            StatsUpdate::Compression(uncomp, comp) => {
                self.uncompressed += into_u32_or_max(uncomp);
                self.compressed += into_u32_or_max(comp);
            },
            StatsUpdate::Reset => *self = Self::default(),
        }
    }

    pub fn compression_ratio(&self) -> u8 {
        if self.uncompressed.clone().bits() == 0 {
            /* sanest default, implies no compression */
            return 100;
        }
        let big_ratio = (self.compressed.clone() * 100u32) / self.uncompressed.clone();
        let digits = big_ratio.to_u32_digits();
        if digits.len() > 1 {
            panic!("Ratio should always be <=100");
        }
        digits[0].try_into().expect("Ratio should always be <=100, which always is in range u8")
    }
}

impl From<&Stats> for crate::Stats
{
    fn from(stats: &Stats) -> Self {
        Self::new(stats.sent(), stats.recieved(), stats.compression_ratio())
    }
}