
// builtin
use std::{
    net::{
        IpAddr,
        SocketAddr,
    },
};

// 3rd party
use log::error;
use simplelog::{
    SimpleLogger,
    LevelFilter,
    Config,
};
use structopt::*;

// internal
use starspawn::{
    server::Server,
    PORT,
};


#[derive(Debug, StructOpt)]
#[structopt(name="starspawn-server")]
pub struct Opt {
    /// Interface to listen on
    #[structopt(default_value="0.0.0.0")]
    interface: IpAddr,
    #[structopt(default_value=PORT)]
    port: u16,
    #[structopt(long, default_value="warn")]
    level: LevelFilter,
}

fn main() {
    let opt = Opt::from_args();
    SimpleLogger::init(opt.level, Config::default()).expect(
        "Couldn't set up logging");

    let server = Server::new(SocketAddr::new(opt.interface, opt.port));
    if let Err(e) = server.run() {
        error!("Error occurred: {}", e);
    }
}
