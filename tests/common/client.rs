// builtin
use std::{
    net::SocketAddr,
    time::Duration,
};

// internal
use starspawn::client::Client;

pub fn get_client(sock: SocketAddr) -> Client {
    Client::connect(sock, Duration::new(10, 0))
        .expect("Should be able to connect to server")
}
