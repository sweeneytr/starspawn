// builtin
use std::{
    net::SocketAddr,
    sync::{
        Arc,
        atomic::{
            AtomicBool,
            Ordering,
        },
    },
    thread,
    time::Duration,
};

// 3rd party
use log::info;

// internal
use starspawn::{
    client::{
        Client,
        Command,
    },
    Request,
};

pub struct Killer<T>
{
    joiner: thread::JoinHandle<T>,
    killer: Box<dyn Fn()>,
    server_addr: SocketAddr,
}

impl<T> Killer<T>
{
    pub fn new(joiner: thread::JoinHandle<T>, killer: Box<dyn Fn()>,
            server_addr: SocketAddr) -> Self {
        Self {
            joiner,
            killer,
            server_addr,
        }
    }

    pub fn kill(self) {
        let timeout = Duration::new(5, 0);
        let mut client = Client::connect(self.server_addr, timeout)
            .expect("Should be able to connect to server");
            
        (self.killer)();
        client.proc_command(Command::Request(Request::Ping)).expect(
            "Should recieve ok response, then server should be exit");
        client.shutdown();
        (self.joiner).join().unwrap();
    }
}

pub type Poisoner = dyn Fn() + Send;
pub type Poisonee = dyn Fn() -> bool + Send;
pub fn poisoners() -> (Box<Poisoner>, Box<Poisonee>) {
    let poison = Arc::new(AtomicBool::new(false));

    let extra_poison = Arc::clone(&poison);
    let poisoner = Box::new(move || {
        info!("Poisioning");
        extra_poison.store(true, Ordering::Relaxed);
    });
    
    let poisonee = Box::new(move || {
        let poisoned = poison.load(Ordering::Relaxed);
        if poisoned {
            info!("Was poisoned");
        }
        poisoned
    });

    (poisoner, poisonee)
}