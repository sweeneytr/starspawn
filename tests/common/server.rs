// builtin
use std::{
    net::{
        IpAddr,
        SocketAddr,
    },
    str::FromStr,
    sync::atomic::{
        AtomicU16,
        Ordering,
    },
    thread,
};

// 3rd party
use lazy_static::lazy_static;

// internal
use starspawn::server::Server;
use super::killer::{
    Killer,
    Poisonee,
    poisoners,
};

lazy_static! {
    static ref SOCK_COUNTER: AtomicU16 = AtomicU16::new(4001);
}

fn server_thread(sock: SocketAddr, halt_hook: Box<Poisonee>)
        -> thread::JoinHandle<()> {
    let builder = thread::Builder::new().name("server_thread".to_string());

    builder.spawn(move || {
        let mut server = Server::new(sock);
        server.add_halt_hook(Box::new(halt_hook))
            .expect("Shouldn't fail, always is first hook installed");
        server.run().unwrap();
    }).unwrap()
}

pub fn run_server() -> (SocketAddr, Killer<()>)  {
    let sock = SocketAddr::new(IpAddr::from_str("127.0.0.1").unwrap(),
        SOCK_COUNTER.fetch_add(1, Ordering::SeqCst));

    let (poisoner, halt_hook) = poisoners();
    let handle = server_thread(sock.clone(), halt_hook);

    (sock, Killer::new(handle, poisoner, sock))
}