
// 3rd party
use log::info;

// internal
mod common;
use starspawn::{
    client::{
        Command,
    },
    Request,
};

#[test]
fn nominal_command_stream() {
    let (sock, killer) = common::run_server();

    let command_stream = vec![
        Command::Request(Request::Ping),
        Command::Request(Request::Compress{string: "aaaaabbbbccccdddeeeageaf".to_string()}),
        Command::Request(Request::Ping),
        Command::Request(Request::GetStats),
        Command::Corrupt,
        Command::Request(Request::Compress{string: "aaaaabbbbccccdddeeeageaf".to_string()}),
        Command::Request(Request::ResetStats),
        Command::Request(Request::Ping),
        Command::Request(Request::GetStats),
    ];

    for command in command_stream.clone().into_iter() {
        let mut client = common::get_client(sock);
        assert!(client.proc_command(command).is_ok());
        client.shutdown();
    }

    let mut client = common::get_client(sock);
    assert!(client.commands(command_stream).is_ok());
    client.shutdown();

    info!("Killing server...");
    killer.kill();
}

#[test]
fn corruption_spam() {
    let (sock, killer) = common::run_server();

    let command_stream = vec![
        Command::Corrupt,
        Command::Corrupt,
        Command::Corrupt,
        Command::Corrupt,
        Command::Corrupt,
    ];

    let mut client = common::get_client(sock);
    assert!(client.commands(command_stream).is_ok());
    client.shutdown();

    info!("Killing server...");
    killer.kill();
}